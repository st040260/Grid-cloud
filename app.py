from flask import Flask, render_template
from flask import send_from_directory
from flask_bootstrap import Bootstrap
from flask_socketio import SocketIO
import zmq

context = zmq.Context()
socket_to_srv1 = context.socket(zmq.REQ)
socket_to_srv1.connect("tcp://127.0.0.1:6001")
socket_to_srv2 = context.socket(zmq.REQ)
socket_to_srv2.connect("tcp://127.0.0.1:6002")

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)
Bootstrap(app)
app.debug = True


def service_invoke(msg):
    if len(msg) < 20:
        socket_to_srv1.send_string(msg)
        return socket_to_srv1.recv_unicode()
    else:
        socket_to_srv2.send_string(msg)
        return socket_to_srv2.recv_unicode()


@app.route('/')
def hello_world():
    return render_template("index.html")


@app.route('/js/<path:path>')
def send_js(path):
    return send_from_directory('static/js', path)


@app.route('/css/<path:path>')
def send_css(path):
    return send_from_directory('static/css', path)


@socketio.on('myConnectEvent')
def handle_my_custom_event(json):
    print('received json: ' + str(json))
    socketio.emit("xtest", "testOK")


@socketio.on('myMessage')
def handle_my_custom_event(json):
    print('received json: ' + str(json))
    socketio.emit("myMessageR", "string={}\t\thash={}".format(json["data"], service_invoke(json["data"])))


if __name__ == '__main__':
    app.run(port=8080)
