import zmq
import hashlib

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://127.0.0.1:6002")

while True:
    msg = hashlib.sha512(socket.recv())
    socket.send_string(msg.hexdigest())

