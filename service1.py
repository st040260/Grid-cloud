import zmq
import hashlib

context = zmq.Context()
socket = context.socket(zmq.REP)
socket.bind("tcp://127.0.0.1:6001")

while True:
    msg = hashlib.sha256(socket.recv())
    socket.send_string(msg.hexdigest())

