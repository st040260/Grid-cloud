import zmq


context = zmq.Context()
socket = context.socket(zmq.REQ)
socket.connect("tcp://127.0.0.1:6001")

for i in range(10):
    msg = "msg {}".format(i)
    socket.send_string(msg)
    print("Sending {}".format(msg))
    msg_in = socket.recv_unicode()
    print("rec = {}".format(msg_in))
